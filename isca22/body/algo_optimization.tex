\section{Algorithmic Optimizations for Sparse Attention}
\label{sec:algorithm_optimization}
%
In this section, we overview \sys algorithmic optimizations for inducing sparsity in attention layers.
%
We first introduce an online pruning method that eliminate \textit{unimportant} attention layer computations as early as possible, merely right after $Score$ calculations (e.g. $\mathcal{Q}\times\mathcal{K}$), to increase the realized performance benefits.
%
Particularly, our method sets the layer-wise pruning thresholds as trainable parameters and jointly fine-tune the model parameters and learn the pruning thresholds as part of a light fine-tuning step.
%
Then, \sys compares the $Score$ = $\mathcal{Q}\times\mathcal{K}$ values against the learned pruning thresholds per attention layer and prunes the ones that satisfy the pruning criteria.
%
Note that, in contrast to prior learned weight pruning method for image classification models ~\cite{learnedthreshold}, the pruning criteria in our work is content-dependant and is applied adaptively based on the calculated $Score$ values.
%
That means the induced sparsity in attention layers by \sys varies from one content to another content.
%
As our results indicate (see Section~\ref{sec:eval}), the adaptive and content-dependant nature of our pruning method enables high sparsity in attention computations while yielding virtually no accuracy loss. 
%

\subsection{Learned Per-Layer Pruning}
% Naive selection of threshold values could hurt accuracy. it doesn't work out of the box
% Our alternative approach to train jointly
% Explain the initial approach.
% But this is non-differentiable
% two design principles
Learning per-layer pruning thresholds for attention layers consists of three main challenges.
%
First, the search space of threshold values is complex and computationally intractable for exhaustive exploration.
%
For example, \bench{BERT-L} model has 24 layers creating a total of 24 threshold parameters, each of which can take any continuous value.
%
Second, simply sweeping the threshold values as a one-time fine-tuning step could negatively affect the model accuracy~\cite{a3:hpca20,spatten:hpca21}.
%
To mitigate these challenges, we propose to jointly fine-tune the model parameters and learn the threshold values as a light fine-tuning step with the joint objective of increasing model sparsity and retaining the baseline model accuracy.
%
However, training the threshold values with the inherently non-differentiable pruning operation poses a unique challenge for gradient-based learned methods.
%
For this, we use an approximate differentiable pruning operation and devise a surrogate regularizer to reinforce sparsity as part of the model loss function.
%
In the following paragraphs, we expound our learned pruning method that couples two design principles, namely ``\textit{pruning with soft threshold}'' and ``\textit{surrogate L$_0$ regularization}''.

\niparagraph{Pruning with soft threshold.}
% Non-differentiable
\begin{figure}[t]
\centering
\subfloat[Ideal Pruning Operation]{\label{fig:thresholding_ideal}\includegraphics[width=0.5\linewidth]{figures/threshold/threshold_baseline.pdf}}
\subfloat[Proposed Pruning with Soft Threshold]{\label{fig:thresholding_soft}\includegraphics[width=0.5\linewidth]{figures/threshold/soft_threshold.pdf}}
\caption{Pruning operation on attention $Score$: (a) ideal magnitude-based pruning operation, (b) proposed differentiable pruning operation with soft threshold.}
\label{fig:thresholding} 
\end{figure}
%
Figure~\ref{fig:thresholding_ideal} demonstrates an ideal pruning operation for $Score$ values (e.g. $Score$ = $\mathcal{Q}\times\mathcal{K}$, where  $\mathcal{Q}$ and $\mathcal{K}$ are $d$-dimension vector corresponding to a single word).
%
The $Score$ values greater than $\mathcal{T}h$ remain unchanged and the $Score$ values less than $\mathcal{T}h$ are clipped to a large negative number.
%
As the pruning operation is followed by a ``\textit{softmax($\cdot$)}'', setting the $Score$ values less than $\mathcal{T}h$ to a large negative number makes the output of the softmax operation sufficiently close to zero. Hence, the large negative numbers are pruned out of the following multiplication into $\mathcal{V}$.
% step function is not differentiable at zero (point the discontinuous point)
However, using this pruning operation as part of a gradient-based training method is not straightforward due to its discontinuity at $X$ = $\mathcal{T}h$.
%

To circumvent the non-differentiality in the pruning operation, we propose to replace this operation with an approximate function that instead uses a soft threshold (shown in Figure~\ref{fig:thresholding_soft}) as follows:
%
\begin{equation}
\label{eq:softhreshold}
    \mathrm{SoftThreshold(x)}= 
    \begin{cases}
    {x~\mathrm{tanh}(s(x-\mathcal{T}h))},&  x \geq \mathcal{T}h\\
    {c~\mathrm{tanh}(s(x-\mathcal{T}h))},  &  x < \mathcal{T}h
\end{cases}
\end{equation}
%
By assigning a reasonably large value to $s$, the shape of $tanh(\cdot)$ around $\mathcal{T}h$ becomes sharper and enables the learning gradients to effectively flow around this region.
% either skip or pruned based on their overal contributions to the loss function.
Supporting the learning gradients to flow at the vicinity of $\mathcal{T}h$ allow the gradient-based learning algorithm to either push down the model parameters (e.g. $\mathcal{Q}$ and $\mathcal{K}$) below the threshold or lift them above the threshold according to their contributions to the overall model accuracy.

Outside the vicinity of $\mathcal{T}h$, the $tanh(\cdot)$ asymptotically approaches one and the ``SoftThreshold'' function simply becomes $\approx{x}$ and $\approx{-c}$ for values $\geq$ $\mathcal{T}h$ and < $\mathcal{T}h$, respectively, which are close approximations of the original pruning operation.
%
In our experiments, we empirically find that setting $c$ = 1000 and $s$ = 10 yield a good approximation for pruning and enables robust training.
%
% class soft_thres_func(torch.autograd.Function):
%     def forward(ctx, x, alpha, s, c):
%         tanh = torch.tanh(s*(x-alpha))
%         coef = torch.where(x > alpha, x, -c)
%         output = coef * tanh
%         return output

\niparagraph{Differentiable surrogate \textit{L}$_0$ regularization.}
% why we need a regularizer? maximize sparsity
Simply using soft threshold as the sole force of pruning does not necessarily reinforce the training method to increase sparsity.
%
Intuitively, the training method may just simply lower the threshold to be a small value, which translates to lower sparsity, in order to maintain high model accuracy.
%
Imposing such constraints to gradient-based methods are generally achieved through adding a regularizer terms to the loss function.
%
A common method to explicitly penalize the number of non-zero model parameters is to use L$_{0}$ regularizer on model parameters in the loss function as follows:
\begin{subeqnarray}
% \begin{split}
    &&\mathrm{L}_{tot}(\theta) = \frac{1}{N}\Big(\mathlarger{\sum}_{i=1}^{N}\mathcal{L}\big(A(x_{i};~\theta),~y_{i}\big)\Big) + \lambda||\theta||_{0} \\
    &&||\theta||_{0} = \mathlarger{\sum}_{j=1}^{|\theta|} \mathbbm{1}[\theta_{j} \ne 0]
% \end{split}
\label{eqn: loss}
\end{subeqnarray}
%
where $\mathcal{L}$ is the model loss function, $A(\cdot)$ is the model output for given input $x_{i}$ and model parameters $\theta$, $y_i$ is the corresponding labeled data, $\lambda$ is the balancing factor for L$_0$ regularizer, and $\mathbbm{1}$ is the identity operator that counts the number of non-zero model parameters.
%

Similar to ``\textit{Threshold}'' function, L$_0$ regularizer suffers from the same non-differentiability limitation.
%
To mitigate this, Louizos et al.~\cite{louizos2017learning} uses a reparameterization of model parameters to compute the training gradients. While this reparameterization technique yields state-of-the-art results for Wide Residual Networks~\cite{wrn:2016} and small datasets, a recent study~\cite{gale2019state} shows that this reparameterization trick performs inconsistently for large-scale tasks such as attention models.
%
In this work, we propose a simple alternative method that uses a differentiable surrogate L$_0$ regularization for the pruning of $Score$ values in attention layers as follows:
%
\begin{subeqnarray}
\label{eq:sparse_count}
    &&||\theta||_{0} = \mathlarger{\sum}_{j=1}^{|score|} \mathbbm{1}[\mathrm{score}_{j} > -c]\\
\label{eq:sparse_count_diff}
    &&||\theta||_{0} \approx \mathlarger{\sum}_{j=1}^{|score|} \mathrm{sigmoid}({k(\mathrm{score}_{j}+c-\alpha}))
\end{subeqnarray}
where $k$ = 100 and $\alpha$ = 1.
%
Using these parameters forces the output of sigmoid($\cdot$) to asymptotically approach one for unpruned $Score$ values and zero for the pruned ones, which are already bounded to $-~c$ as shown in Equation~\ref{eq:softhreshold}.
%
As such, the proposed differentiable surrogate L$_0$ regularizer is a close approximation of the original L$_{0}$ regularizer in Equation~\ref{eq:sparse_count}~(a).
%

\niparagraph{\sys pruning mechanism.}
%
We apply our gradient-based learned pruning as a light fine-tuning step based on the previously proposed design principles: (1) pruning with soft threshold and (2) differentiable surrogate \textit{L}$_0$ regularization.
%
We employ the pre-trained attention models with the proposed modified loss function (e.g. original loss function and the surrogate L$_0$ regularizer) to jointly fine-tune the model parameters and learn the per-layer pruning thresholds.
%
Using the proposed soft threshold mechanism in the fine-tuning step allows the gradient-based learning method to adjust the model parameters smoothly at the vicinity of the $\mathcal{T}h$ value.
%
That is, pushing down the non-important model parameters below threshold values and lifting up the important model parameters above it.

One of the main benefit of using the proposed differentiable approach is enabling the model parameters to freely switch between prune and unpruned region.
%
For all the studied attention models, we initialize the threshold values to zero and run the fine-tuning for up to five epochs.
%
We provide the details of training hyperparameters in Section~\ref{sec:eval}.

Figure~\ref{fig:training_curve} demonstrates an example sparsity, threshold values, and normalized training loss curves for \bench{BERT-B} model on QNLI task from the GLUE benchmark.
%
Figure~\ref{fig:curve_sparsity_threshold} shows that as fine-tuning epochs progress, both the sparsity and threshold values increase owing to the effectiveness of our joint co-training of sparsity and model parameters.
%
The flexibility afforded by the joint co-training is further illustrated at the third epoch, where the sparsity continues to increase despite the corresponding decrease in the threshold value.
%
Additionally, Figure~\ref{fig:curve_training_loss} shows the decreasing trend of normalized training loss over the course of fine-tuning epochs.
% Threshold: 1e-2, others: 5e-6.
% 1:09
% For the acc_loss and sparsity_loss, total_loss = acc_loss + 1000*sparsity_loss

% Mingu Kang  1:15 PM
% Decay parameters = 0
% 1:17
% batch_size = 16
% 1:17
% warmup = 0.1
% 1:17
% seed = 42
% 1:17
% epochs = 1 - 5
\begin{figure}[t]
\centering
\subfloat[Sparsity and Pruning Threshold]{\label{fig:curve_sparsity_threshold}\includegraphics[width=0.48\linewidth]{figures/curve_sparsity_threshold.pdf}}
\subfloat[Normalized Training Loss]{\label{fig:curve_training_loss}\includegraphics[width=0.47\linewidth]{figures/curve_training_loss.pdf}}
\caption{An example (a) attention layer sparsity and its corresponding pruning threshold values and (b) normalized training loss as fine-tuning epochs progress for \bench{BERT-L} model on QNLI task from GLUE benchmark.}
\label{fig:training_curve} 
\end{figure}
%
\subsection{Bit-Level Early-Compute Termination}
\label{sec:early_stop}
% the learned threshold provides an opportunity to further reduce the computations.
The learned pruning offers an interesting opportunity to further improve the \sys performance through bit-serial $\mathcal{Q}\times\mathcal{K}$ computation.
%
If our system can anticipate that the final result of $\mathcal{Q}\times\mathcal{K}$ computation is below the learned pruning threshold, the ongoing bit-serial computations can be terminated.
%
However, this early-termination mechanism poses a key challenge in our design.
%
As we desire to maintain the baseline model accuracy, the early-termination mechanism must not tamper with the computational correctness of attention layers.
%
To address this, we propose to compute and add a dynamically adjusted conservative margin value to the partial sum during the bit-serial computations. 
%
The role of this margin is to account for the maximum potential increase in the remaining $\mathcal{Q}\times\mathcal{K}$ computations.
%
If the addition of the partial sum values and the margin still falls below the learned pruning threshold value, the computations are terminated and the corresponding $\mathcal{Q}\times\mathcal{K}$ is simply pruned.
%
In the following paragraph, we illustrate the proposed early-compute termination with a conservative margin.
% fixed-point into bit serial
% potential increase and margin calculation
% we don't want error in model accuracy, hence conservative margin calculation
\if 0
\niparagraph{Early-compute termination with conservative margin.}
\label{sec:margin_est}
% Strategy to maintain the model accuracy and correctness of computation
% As mentioned, the learned pruning unlocks a promising opportunity to terminate the $\mathcal{Q}\times\mathcal{K}$ computations earlier in favor of higher computational efficiency.
%
%To leverage this opportunity, 
We propose to perform the $\mathcal{Q}\times\mathcal{K}$ computations in a bit-serial manner.
%
That is, we maintain the query ($\mathcal{Q}$) and key values ($\mathcal{K}$) in full-precision and bit-serial fixed-point structures, respectively.
%
Each cycle, a group of $\mathcal{K}$ bits (from MSB to LSB) is multiplied by $\mathcal{Q}$ and the partial results are iteratively updated as shown in Equation~\ref{eq:bitserial}.
%
$\mathcal{K}$ is a fixed-point number with m integer bits and n fractional bits. For simplicity, we assume there is only one bit per group and both numbers are positive.
%
\begin{equation}
\label{eq:bitserial}
\begin{split}
    \mathcal{Q}\times\mathcal{K} = & \mathcal{Q}\times\mathlarger{\sum}_{i=m}^{-n} k_{i}~2^{i}\\
    = &~\mathcal{Q}\times(k_{m}~2^{m}) + \cdot\cdot\cdot + \mathcal{Q}\times(k_{-n}~2^{-n})
\end{split}
\end{equation}

\begin{table}[t!]
\footnotesize{
\centering
\vspace*{0.1cm}
\caption{\label{table:early_terminate}High-level overview of bit-level early compute termination for $\mathcal{Q}\times\mathcal{K}$ computations. The second and third column depicts the partial sum and the proposed conservative margin values, respectively. The final column shows whether the early terminate criteria is satisfied. \scriptsize{\XSolidBrush} implies that the early termination criteria  is not satisfied, whereas \scriptsize{\CheckmarkBold} indicates the engagement of early-compute termination mechanism. In this example, the early terminate mechanism is engaged after $c$ cycles (highlighted in gray).}
\resizebox{0.49\textwidth}{!}{% <------ Don't forget this %
\begin{tabular}{l|l|l|l}
\bottomrule
\textbf{Cycle}&\textbf{$\mathcal{P}$ = Partial Sum}&\textbf{$\mathcal{M}$ = Conservative Margin}&\textbf{Early Termination?}\\\bottomrule
1&$\mathcal{P}_1 = \mathcal{Q}\times(k_{m}~2^{m})$&$\mathcal{M}_1 = \mathcal{Q}\times\mathlarger{\sum}_{i=m-1}^{-n} 2^{i}$&$\mathcal{P}_1 + \mathcal{M}_1$ $\geq$ $\mathcal{T}h$;~\normalsize{\XSolidBrush}\\\midrule
2&$\mathcal{P}_2 = \mathcal{P}_1 + \mathcal{Q}\times(k_{m-1}~2^{m-1})$&$\mathcal{M}_2 = \mathcal{Q}\times\mathlarger{\sum}_{i=m-2}^{-n} 2^{i}$&$\mathcal{P}_1 + \mathcal{M}_1$ $\geq$ $\mathcal{T}h$;~\normalsize{\XSolidBrush}\\\midrule
$\cdot\cdot\cdot$&$\cdot\cdot\cdot$&$\cdot\cdot\cdot$&$\cdot\cdot\cdot$\\\midrule
c&$\mathcal{P}_c = \mathcal{P}_1 + \cdot\cdot\cdot\mathcal{P}_{c-1}+\mathcal{Q}\times(k_{m-c}~2^{m-c})$&$\mathcal{M}_c = \mathcal{Q}\times\mathlarger{\sum}_{i=m-c-1}^{-n} 2^{i}$&$\mathcal{P}_c + \mathcal{M}_c$ < $\mathcal{T}h$;~\normalsize{\CheckmarkBold}\\\bottomrule
\end{tabular}% <------ Don't forget this %
}
}
\end{table}
%
\noindent{}Following this approach, we can terminate the computations as soon as we anticipate that the final result will \textit{never} cross the pruning threshold boundary.
%
As illustrated in Table~\ref{table:early_terminate}, our strategy is to conservatively compute a dynamically adjusted margin by presuming that all the remaining bits in $\mathcal{K}$---the ones that are not yet multiplied by $\mathcal{K}$---are one.
%
Adding this margin with the current partial sum value yields an upper bound on the final value of $\mathcal{Q}\times\mathcal{K}$.
%
If this upper bound is still below the learned pruning threshold, we can simply terminate the following computations early in favor of further performance benefits. 
%
% Note that, the requirement of maintaining model accuracy in this work imposes this conservative constraint on margin calculation.
\fi

\niparagraph{Early-compute termination for dot-product operation.}
% Multiple values
% In this section, we elaborate on the algorithmic mechanism of early-compute termination for $\mathcal{Q}\times\mathcal{K}$ dot-product operation.
%
Figure~\ref{fig:bit_serial_alg_overview} depicts the flow for a $\mathcal{Q}\times\mathcal{K}$ dot-product computation, each with four elements.
%
$\mathcal{K}$ elements are placed in bit-serial format vertically from MSB $\rightarrow$ LSB, whereas $\mathcal{Q}$ values are stored in full-precision fixed-point format. In this example, the threshold value is set to five.
%
For simplicity, we assume the computation is performed in sign-magnitude form, $k_s$ represents the sign-bit for $\mathcal{K}$ vector, and the absolute values of $\mathcal{K}$ elements are less than one.

In the first cycle, before the computation starts, the elements with concordant signs, ($k^0$, $q^0$) and ($k^1$, $q^1$), are used for margin initialization.
%
The intuition here is that \textit{only} the multiplications of elements with concordant signs can contribute positively to the final dot-product result.
%
Multiplications of elements with opposing signs are ignored to keep the margin conservative and eliminate wrongful early compute terminations.
%
As shown in the table of Figure~\ref{fig:bit_serial_alg_overview}, both the product of $k^2$ and the $q$ vector as well as the margin are updated.
%
The margin is adjusted to accommodate the largest possible positive contribution to the final value.
%
In the second cycle, because the sum of $\mathcal{P}_2$ and $\mathcal{M}_2$ dips below the threshold, the computation process terminates. That is, the subsequent cycles (highlighted in gray) are no longer performed.
%
Note that, with the proposed margin computation, we ensure that no approximation is introduced in the attention layers. 
%
In the next section, we discuss the details of the microarchitectural realization of early-compute termination.
\begin{figure}
% \begin{table}[t!]
\subfloat[Cycle = 1]{\label{fig:step_1}\includegraphics[width=0.24\linewidth]{figures/bit_serial_alg/step_1.pdf}}
\subfloat[Cycle = 2]{\label{fig:step_2}\includegraphics[width=0.24\linewidth]{figures/bit_serial_alg/step_2.pdf}}
\subfloat[Cycle = 3]{\label{fig:step_2}\includegraphics[width=0.24\linewidth]{figures/bit_serial_alg/step_3.pdf}}
\subfloat[Cycle = 4]{\label{fig:step_2}\includegraphics[width=0.24\linewidth]{figures/bit_serial_alg/step_4.pdf}}
\footnotesize{
\centering
\vspace*{0.1cm}
\resizebox{0.49\textwidth}{!}{% <------ Don't forget this %
\begin{tabular}{l|l|l|l}
\bottomrule
\textbf{Cycle}&\textbf{$\mathcal{P}$ = Partial Sum}&\textbf{$\mathcal{M}$ = Conservative Margin}&\textbf{Early Termination? ($\mathcal{T}h$ = 5)}\\\bottomrule
1&$\mathcal{P}_1 = 0$&$\mathcal{M}_1 = (9+5)(2^{-1}+2^{-2}+2^{-3}) = 12.25$&$\mathcal{P}_1 + \mathcal{M}_1 = 12.25 \geq 5$;~\normalsize{\XSolidBrush}\\\midrule
2&$\mathcal{P}_2 = \mathcal{P}_1 + (5-7)2^{-1} = -1$&$\mathcal{M}_2 = (9+5)(2^{-2}+2^{-3}) = 5.25$&$\mathcal{P}_2 + \mathcal{M}_2 = 4.25 <$ $5$;~\normalsize{\CheckmarkBold}\\\midrule
\CC~3&\CC~$\mathcal{P}_3 = \mathcal{P}_2 + (5-2)2^{-2} = -0.25$&\CC~$\mathcal{M}_3 = (9+5)(2^{-3}) = 1.75$&\CC~$\mathcal{P}_3 + \mathcal{M}_3 = 1.5 < $ $5$;\\\midrule
\CC~4&\CC~$\mathcal{P}_4 = \mathcal{P}_3 + (9+5)2^{-3} = 1.5$&\CC~$\mathcal{M}_4 = 0$&\CC~$\mathcal{P}_4 + \mathcal{M}_4 = 1.5 < $ $5$;\\\bottomrule
\end{tabular}% <------ Don't forget this %
}
}
\caption{\label{fig:bit_serial_alg_overview}High-level overview of early-compute termination for dot-product operation $\mathcal{Q}\times\mathcal{K}$. In this example, $\mathcal{K}$ is represented in bit-serial format, whereas $\mathcal{Q}$ is in full-precision fixed-point format. In Figure (a-d) each column illustrate one element of $\mathcal{K}$ vector and each row represents its corresponding bits (MSB $\rightarrow$ LSB). K$_{s}$ indicates the sign bit. For simplicity, $\mathcal{K}$ elements are scaled to be between -1.0 and +1.0. The table shows the partial sum values after each cycle. The early-termination gets engaged in the second cycle and the remaining computations (at third and fourth cycles) will be disregarded.}
\end{figure}


% \begin{figure}
% \centering{}\includegraphics[width=0.95\columnwidth]{figures/bit_serial_example.pdf}
% \caption{Bit serial computation.}
% \label{fig:bit_serial_bounding} 
% \end{figure}

% \begin{figure}
% \centering{}\includegraphics[width=0.95\columnwidth]{figures/nlp_computation.pdf}
% \caption{Computation in attention mechanism and large redundancy.}
% \label{fig:sentence_redundancy} 
% \end{figure}

\if false
\subsection{Early-stop with bit sequential processing of Q*K}\label{sec:early_stop}
To prune out the redundant portion of data and its computation as early as possible, we employ bit sequential processing in the vector multiplication computation between queue (Q) and key (K) vectors\amir{cite snapea,bitfusion,stripe}. Here, the key vectors are processed bit-sequential manner from MSB to LSB order while the queue is still maintained to be decimal. Thus, the multiplication between binary vs. decimal vectors is processed at each cycle, and the result is binary-scaled to represent the bit position of key, and accumulated across cycles to produce the psum\amir{add a figure to clarify the text.}. 
During the bit-sequential process, the $psum(b)$ is compared with a threshold at the end of $b$-th bit processing to check the early-stop possibility by pruning as follows:
\begin{equation}
psum(b)+margin(b)\underset{early-stop}{\overset{continue}{\gtrless}} th\label{eqn: decision}
\end{equation}
As the $psum$ can be increased during the processing of remaining bit positions,  the $margin(b)$ is added to $psum$ to consider the potential increase during the decision in (\ref{eqn: decision}). The $margin(b)$ is pre-calculated
to consider the largest possible additive value for the $psum$ during the remaining bit processing stages. If the $psum(b)$ is smaller than $th$ even in consideration of $margin(b)$, the bit-sequential processing is early-stopped to be pruned out. Otherwise, the computation for the remaining bit-position is continued.   

\subsection{Margin estimation for Early stop}\label{sec:margin_est}
During this process, it is important to find the tight enough $margin(b)$ to prune out as early as possible with minimum bit computations while guaranteeing the computational correctness. To do so, we propose the following procedure with an example in Fig.~\ref{fig:bit_serial_bounding}:


\noindent $\bullet$ Step 1) During the 0-th (sign) bit processing, find the  $\mathrm{idx}_{pos}$ vector, which is the same length with queue and key vectors. This vector's element is '1' if the signs of key and queue are same at the position in the vector whereas the other elements are all '0's. Therefore, $\mathrm{idx}_{pos}$ vector indicates that the product vector's element has a positive value at the position when $\mathrm{idx}_{pos}$ is '1', and only these positions will contribute to increase the $psum(b)$ as $b$ increases. 

\noindent $\bullet$ Step 2) Compute the sum ($Q_{pos\_sum}$) of absolute value of queue elements at the location of $\mathrm{idx}_{pos}$='1'.

\noindent $\bullet$ Step 3) Compute the margin value used for $b$-th bit processing as follows:
\begin{equation}
margin(b) = Q_{pos\_sum} * \sum_{x=b+1}^{B-1} 2^{-x}    \label{eqn: margin}
\end{equation}
Here, the normalized value of queue element, e.g., magnitude is smaller than 1, is assumed for the simplicity. The term $\sum_{x=b+1}^{B-1} 2^{-x}$ is the largest possible number which can be created with the remaining bit positions after finishing $b$-th bit processing. 

Above computations to prepare $margin(b)$ are processed only during the 0-th (sign) bit processing. During the subsequent bit-position processing, the pre-computed $margin(b)$ values are employed for the early-stop decision in (\ref{eqn: decision}). Whereas the  $\sum_{x=b+1}^{B-1}$ can be pre-computed and stored in the look-up-table, the $Q_{pos\_sum}$ needs to be computed at every 0-th bit position processing. It can be avoided by using  $Q_{sum}$, which is the sum of all the elements' absolute values in queue vector, instead of using $Q_{pos\_sum}$. The $Q_{sum}$ can be re-used across all the key vectors, e.g., 128 - 512 vectors, once it is calculated as a pre-processing stage. In this case, the margin is less tight as compared to the $Q_{pos\_sum}$. Thus, a couple of more bits need to be processed before the early-stop happens.
\amir{this makes sense as this is an upper bound. we can start with this and then propose our solution.}

Furthermore, the NLP models are highly sensitive\amir{add citations} to the subtle modifications.
The following paragraphs discuss alternative approaches to address the issues.

%
In the NLP models with early-pruning capability,  thresholding function is required for $Score$s right after QK computations. Note that the thresholding function is followed by softmax, which includes the exponent ($e^x$) computations. To prune out the redundant small values from the softmax computation, the  thresholding function (Fig.~\ref{fig:thresholding}(a)) should be as follows:

\[
    thres(x)= 
\begin{cases}
    {x},&  x > th\\
    th,  &  x  = th \\
    -c, &  x < th 
\end{cases}
\]
where $c$ is large enough value to make $e^{(-c)}$ converge to zero. As the derivative of $thres(x)$ is non-zero only at the point $x=th$, the training is very slow as the event that $x$ is exactly $th$ happens very sparsely\amir{We should add ablation study for both options.}. 
As a solution, we propose soft-threshold as below:
\[
    softthres(x)= 
\begin{cases}
    {x},&  x > th\\
    sigm(s(x-th))-c,  &  x \leq th
\end{cases}
\]
As shown in Fig.~\ref{fig:thresholding}(b), the $softthres(x)$ is a good approximation of $thres(x)$ other than the points near $Th$. By maintaining the $s$ to be large enough, e.g., $>$10, the sigmoid becomes sharp making $softthres(x)$ be close enough to the ideal $thres(x)$ function. In addition, the derivative of $softthres(x)$ is non-zero in the range of $x \leq th$ and can be calculated easily, which facilitates the training by including the $softthres(x)$ function.

%
\begin{figure}[t]
\centering
\subfloat[Ideal Pruning Operation]{\label{fig:thresholding_ideal}\includegraphics[width=0.5\linewidth]{figures/threshold/threshold_baseline.pdf}}
\subfloat[Proposed Pruning with Soft Threshold]{\label{fig:thresholding_soft}\includegraphics[width=0.5\linewidth]{figures/threshold/soft_threshold.pdf}}
\caption{Pruning operation on attention $Score$: (a) ideal magnitude-based pruning operation, (b) proposed differentiable pruning operation with soft threshold.}
\label{fig:thresholding} 
\end{figure}
% \begin{figure}
% \centering{}\includegraphics[width=0.95\columnwidth]{figures/soft_count.pdf}
% \caption{Example of soft-count.}
% \label{fig:soft_count} 
% \end{figure}
% %






\begin{figure}
\centering{}\includegraphics[width=0.95\columnwidth]{figures/nlp_computation.pdf}
\caption{Computation in attention mechanism and large redundancy.}
\label{fig:sentence_redundancy} 
\end{figure}

\begin{figure}
\centering{}\includegraphics[width=0.75\columnwidth]{figures/bert_power_breakdown.pdf}
\caption{Power breakdown in the self-attention computation.}
\label{fig:bert_power_breakdown} 
\end{figure}

\begin{figure}
\centering{}\includegraphics[width=0.95\columnwidth]{figures/train_method.pdf}
\caption{Training method.}
\label{fig:train_method} 
\end{figure}




\noindent $\bullet$ Soft-thresholding: 
In the NLP models with early-pruning capability,  thresholding function is required for $Score$s right after QK computations. Note that the thresholding function is followed by softmax, which includes the exponent ($e^x$) computations. To prune out the redundant small values from the softmax computation, the  thresholding function (Fig.~\ref{fig:thresholding}(a)) should be as follows:

\[
    thres(x)= 
\begin{cases}
    {x},&  x > th\\
    th,  &  x  = th \\
    -c, &  x < th 
\end{cases}
\]
where $c$ is large enough value to make $e^{(-c)}$ converge to zero. As the derivative of $thres(x)$ is non-zero only at the point $x=th$, the training is very slow as the event that $x$ is exactly $th$ happens very sparsely\amir{We should add ablation study for both options.}. 
As a solution, we propose soft-threshold as below:
\[
    softthres(x)= 
\begin{cases}
    {x},&  x > th\\
    sigm(s(x-th))-c,  &  x \leq th
\end{cases}
\]
As shown in Fig.~\ref{fig:thresholding}(b), the $softthres(x)$ is a good approximation of $thres(x)$ other than the points near $Th$. By maintaining the $s$ to be large enough, e.g., $>$10, the sigmoid becomes sharp making $softthres(x)$ be close enough to the ideal $thres(x)$ function. In addition, the derivative of $softthres(x)$ is non-zero in the range of $x \leq th$ and can be calculated easily, which facilitates the training by including the $softthres(x)$ function.

\begin{figure}
\centering{}\includegraphics[width=0.95\columnwidth]{figures/soft_count.pdf}
\caption{Example of soft-count.}
\label{fig:soft_count} 
\end{figure}



\noindent $\bullet$ Loss generation for sparsity-aware training: To train the NLP model to increase the sparsity with optimal threshold, the loss function for the training should include both accuracy and non-sparsity information. Thus, the total loss is defined as\amir{Does it make sense to have a weight for each element? Also provide intuition about the loss function.  -> Mingu: Yes, indeed there is an balancing parameter gamma. The intuition is described right below the equation}:
\begin{equation}
    loss=loss_{acc} + \gamma \times loss_{non\_sparsity} \label{eqn: loss}
\end{equation}
By defining the loss function as above, the training will consider both the accuracy and sparsity simultaneously. However, if one of those are significantly larger, the training process will optimize only the outstanding loss. Therefore, a  coefficient $\gamma$ is applied to balance the magnitudes of two losses. Here, the $loss_{non\_sparsity}$ is supposed to reflect the number of un-pruned elements, which is calculated as below: 
\begin{equation}
    loss_{non\_sparsity}=\mathrm{count_i}[score_i>-c] \label{eqn: non_sparsity}
\end{equation}
However, above count function is not differentiable, which makes it difficult to calculate the gradient. As a solution, we propose a soft-count function as below:
\begin{equation}
    \mathrm{soft\_count}_i(score_i)= sum_i[sigm[score_i+c-1]] \label{eqn: soft_counter}
\end{equation}
Note $c$ is large enough number. Therefore, the un-pruned numbers are converged to 1 whereas pruned numbers to 0 as shown in Fig.~\ref{fig:soft_count} through $sigm[score_i+c-1]$. By summing up the elements, $\mathrm{soft\_count}()$ outputs the number of non-zero elements. As $\mathrm{soft\_count}()$ consists of sigmoid and linear operations, the derivative can be calculated easily for the gradient generation.

\subsection{Training method}\label{sec: train_method}
It is known that training of NLP models such as BERT takes long due to the large model size and numerous trainable parameters. Therefore, it is common to employ a pre-trained BERT model as a starting point and it is  fine-tuned for various applications with specific data sets.
Our training for the sparsity is completely compatible with the pre-train model 
without requiring the training from scratch. 
The training process is described in Fig.~\ref{fig:train_method}, where the sparsity-aware training can be performed by inheriting the trained parameters from the fine-tuning stage. At the beginning of the sparsity-aware training, we set the threshold to be zero. While the training goes on, the sparsity goes up and the  threshold is also updated by co-optimizing both the sparsity and accuracy as shown in Fig.~\ref{fig:training_curv}, where the example training curve for GLUE QNLI dataset is depicted in Fig.~\ref{fig:training_curv}. The sparsity-aware training requires 5 additional epochs.
In this training, we constrained the  thresholds for all the heads to be same within a layer for the simplicity\amir{Does it provide any benefit to have per layer sparsity?}. It is allowed to use different thresholds for different layers. Thus, once the sparsity-aware training is completed, optimal thresholds are generated for each layer, e.g., 24 threshold values for BERT large for 24 layers, along with the other trained parameters.


\begin{figure}[t]
\centering
\subfloat[]{\includegraphics[width=0.75\columnwidth]{figures/ideal_thres.pdf}}
\hfill
\subfloat[]{\includegraphics[width=0.7\columnwidth]{figures/soft_thres.pdf}}
\caption{Thresholding operations: (a) ideal thresholding, (b) proposed soft-thresholding for back propagation.}
\label{fig:thresholding} 
\end{figure}



\subsection{Early-stop with bit sequential processing of Q*K}\label{sec:early_stop}
To prune out the redundant portion of data and its computation as early as possible, we employ bit sequential processing in the vector multiplication computation between queue (Q) and key (K) vectors\amir{cite snapea,bitfusion}. Here, the key vectors are processed bit-sequential manner from MSB to LSB order while the queue is still maintained to be decimal. Thus, the multiplication between binary vs. decimal vectors is processed at each cycle, and the result is binary-scaled to represent the bit position of key, and accumulated across cycles to produce the psum\amir{add a figure to clarify the text.}. 
During the bit-sequential process, the $psum(b)$ is compared with a threshold at the end of $b$-th bit processing to check the early-stop possibility by pruning as follows:
\begin{equation}
psum(b)+margin(b)\underset{early-stop}{\overset{continue}{\gtrless}} th\label{eqn: decision}
\end{equation}
As the $psum$ can be increased during the processing of remaining bit positions,  the $margin(b)$ is added to $psum$ to consider the potential increase during the decision in (\ref{eqn: decision}). The $margin(b)$ is pre-calculated
to consider the largest possible additive value for the $psum$ during the remaining bit processing stages. If the $psum(b)$ is smaller than $th$ even in consideration of $margin(b)$, the bit-sequential processing is early-stopped to be pruned out. Otherwise, the computation for the remaining bit-position is continued.   



\begin{figure}
\centering{}\includegraphics[width=0.95\columnwidth]{figures/training_curve.pdf}
\caption{Example of training curve (\amir{which layer it is?}) with GLUE QNLI dataset. How many epochs? why accuracy drop is high?}
\label{fig:training_curv} 
\end{figure}

\subsection{Margin estimation for Early stop}\label{sec:margin_est}
During this process, it is important to find the tight enough $margin(b)$ to prune out as early as possible with minimum bit computations while guaranteeing the computational correctness. To do so, we propose the following procedure with an example in Fig.~\ref{fig:bit_serial_bounding}:


\noindent $\bullet$ Step 1) During the 0-th (sign) bit processing, find the  $\mathrm{idx}_{pos}$ vector, which is the same length with queue and key vectors. This vector's element is '1' if the signs of key and queue are same at the position in the vector whereas the other elements are all '0's. Therefore, $\mathrm{idx}_{pos}$ vector indicates that the product vector's element has a positive value at the position when $\mathrm{idx}_{pos}$ is '1', and only these positions will contribute to increase the $psum(b)$ as $b$ increases. 

\noindent $\bullet$ Step 2) Compute the sum ($Q_{pos\_sum}$) of absolute value of queue elements at the location of $\mathrm{idx}_{pos}$='1'.

\noindent $\bullet$ Step 3) Compute the margin value used for $b$-th bit processing as follows:
\begin{equation}
margin(b) = Q_{pos\_sum} * \sum_{x=b+1}^{B-1} 2^{-x}    \label{eqn: margin}
\end{equation}
Here, the normalized value of $\mathcal{Q}$ element, e.g., magnitude is smaller than 1, is assumed for the simplicity. The term $\sum_{x=b+1}^{B-1} 2^{-x}$ is the largest possible number which can be created with the remaining bit positions after finishing $b$-th bit processing. 

Above computations to prepare $margin(b)$ are processed only during the 0-th (sign) bit processing. During the subsequent bit-position processing, the pre-computed $margin(b)$ values are employed for the early-stop decision in (\ref{eqn: decision}). Whereas the  $\sum_{x=b+1}^{B-1}$ can be pre-computed and stored in the look-up-table, the $Q_{pos\_sum}$ needs to be computed at every 0-th bit position processing. It can be avoided by using  $Q_{sum}$, which is the sum of all the elements' absolute values in queue vector, instead of using $Q_{pos\_sum}$. The $Q_{sum}$ can be re-used across all the key vectors, e.g., 128 - 512 vectors, once it is calculated as a pre-processing stage. In this case, the margin is less tight as compared to the $Q_{pos\_sum}$. Thus, a couple of more bits need to be processed before the early-stop happens.
\amir{this makes sense as this is an upper bound. we can start with this and then propose our solution.}

Furthermore, the NLP models are highly sensitive\amir{add citations} to the subtle modifications.
The following paragraphs discuss alternative approaches to address the issues.

% The early-stage on-line pruning requires finding  thresholds to prune out the small enough value after QK process. However, it is non-trivial to find the optimal threshold to maximize the sparsity while guaranteeing the accuracy. 
% %
% There are many layers (e.g., 16) and multiple heads (e.g., 12 in BERT) creating 192 combinations. Therefore, it is not feasible to search the optimal threshold for each individual head by sweeping manually in a brute-force manner.
% %
% As an alternative approach, we train the threshold along with the other weights during the re-training process.
%
% In this process, we consider the loss for the classification accuracy ($loss_{acc}$) and the number of un-pruned values ($loss_{non\_zero}$) after QK processing. This training process tries to boost the sparsity while maintaining the sparsity by co-optimizing the threshold and other trainable parameters.
%
% However, it is non-trivial to train the threshold as a parameter because the thresholding function and the loss for non-zero elements are inherently non-differentiable functions, which is difficult to calculate the gradient.

\fi