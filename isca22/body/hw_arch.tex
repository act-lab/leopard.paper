\section{\sys Hardware architecture}
\label{sec:hw_arch}

\begin{figure}[t]
\centering
\includegraphics[width=0.7\columnwidth]{figures/arch.pdf}
\caption{Overall microarchitecture of a \sys tile.}
\label{fig:architecture} 
\end{figure}
%
This section delves into the innards of \sys accelerator microarchitecture.
%
We design \sys hardware while considering the following requirements based on our algorithmic optimizations:
%
\begin{enumerate}
    \item Leveraging the layer threshold values to detect the unpruned $Score$s and their corresponding indices in the output matrix.
    %
    \item Using bit-serial processing to early-stop the computation of pruned $Score$s and associated memory access .
    %
    \item Processing the $\times \mathcal{V}$ operation for only un-pruned $Score$s to minimize operations while achieving high compute utilization.
\end{enumerate}
%
With these considerations in mind, we discuss the overall architecture of the \sys accelerator.
%
\subsection{Overall Architecture}
\label{sec:overall_arch}
%
Due to abundant available parallelism in multi-head attention layers, we design a tile-based architecture for \sys, where attention heads are partitioned across the tiles, and the operations in the tiles are independent of each other on their corresponding heads.
%
Figure~\ref{fig:architecture} illustrates the high-level microarchitecture of a single \sys tile.
%
Each tile comprises two major modules to process the computations of attention layers:
%
(1) A front-end unit, dubbed Query Key Processing Unit (\qkvxu), that streams in the $\mathcal{Q}$ vectors (row by row of the $Q$ matrix, where each row corresponds to each word) from the off-chip memory, reads $\mathcal{K}$s from a local buffer, and performs vector-matrix multiplication operations between a $\mathcal{Q}$ vector and a $\mathcal{K}$ matrix.
%
This unit also encompasses a 1-D array of bit-serial dot-product units, \code{QK-DPU}s, each of which equipped with logic to early-stop the computations based on the pruning threshold values and forward the unpruned $Score$s and their indices to the second stage.
%
(2) A back-end unit, dubbed Value Processing Unit (\vvxu), that performs softmax operations on the important un-pruned $Score$s to generate probability, and subsequently performs weighted-summation of the $\mathcal{V}$ vectors  read from a local buffer to generate the final output of the attention layer.
The front- and back-end stages are connected to each other through a set of FIFOs that store the survived $Score$s and their corresponding indices. 
The front-end unit employs multiple ($N_{\mathrm{QK}}$) \code{QK-DPU}s while sharing the single \vvxu in consideration of high pruning rate during the processing in the front-end stage.
%
If the front-end finished the computation with current $\mathcal{Q}$ vector, but the back-end is still working on the previous $\mathcal{Q}$ vector, the front-end unit is stalled until the completion of back-end unit.
%On the other hand, if the pruning ratio is very high from the front-end unit, the back-end would be idle waiting for the next input.
%
As the choice of $N_{\mathrm{QK}}$ is a key factor to maximize the  overall throughput and back-end resource utilization, we explore this design space in Section~\ref{sec:design_space}, which leads to two choices of $N_{\mathrm{QK}}=$ 6 and 8 by focusing on area efficiency and higher utilization.

Before the operation begins, all the $\mathcal{K}$ and $\mathcal{V}$ matrices for a layer are fetched from off-chip memory and stored on their dedicated on-chip buffers, while the $\mathcal{Q}$ vectors are steamed in.
%
Note that the vectors are re-used by the number of sequences (e.g., 512 in BERT models) amortizing the DRAM access costs.
%
% Below we discuss the details of the aforementioned microarchitectural units.
%
\subsection{Online Pruning Hardware Realization via Bit-serial Execution}
%
% first explain what we want to achieve briefly and reference to algorithmic optimization and then say QK-DPU does this and then explain each parts of it.
As discussed in Section~\ref{sec:algorithm_optimization}, to realize the pruning of redundant $Score$s during runtime and even earlier termination  with the fine granularity of bit-level, we design \sys front-end unit (depicted in Figure~\ref{fig:architecture}-(a)) as a collection of bit-serial dot-product units (\code{QK-DPU}), that cooperatively multiply a vector of $\mathcal{Q}$ with the $\mathcal{K}$ matrix.
%

\niparagraph{Overall front-end execution flow.}
%
To perform the $Score$ computations, the $\mathcal{Q}$ vectors are read sequentially from \code{Q-FIFO} and then broadcasted to each \code{QK-DPU}, while each \code{QK-DPU} reads a $\mathcal{K}$ vector from its local \code{Key Buffer} and performs a vector dot-product operation.
%
As such, while the $\mathcal{Q}$ vector is shared amongst the \code{QK-DPU}s, the $\mathcal{K}$ matrix is partitioned along its columns and is distributed across the \code{Key Buffer}s to extract data-level parallelism.
%
Each \code{QK-DPU} performs the dot-product operations in a \emph{bit-serial} mode, where the $\mathcal{K}$ elements are processed in bit-sequential manner and the $\mathcal{Q}$ elements are processed as a whole (e.g. 12 bit).
%
Whenever each \code{QK-DPU} finishes the processing of all its $\mathcal{K}$ bits for unpruned $Score$s or early terminates the computation due to not meeting the layer pruning threshold based on the margin calculation described in Section~\ref{sec:early_stop}, it proceeds with the execution of next $\mathcal{K}$ vector.
%
If a \code{QK-DPU} detects a unpruned $Score$, it stores the $Score$ value and its corresponding index on \code{Score-FIFO} and \code{IDX-FIFO}, respectively, to be processed by the back-end unit later.
%
Once all the \code{QK-DPU}s finish processing all their $\mathcal{K}$ vectors, the \code{QK-PU} reads the next $\mathcal{Q}$ vector from \code{Q-FIFO} and starts its processing.
%
%Once one of the QK processor completes with all the $M$ key vectors, the QK processor begins the computation with the next streamed in queue vector $Q_{(n+1)}$. 
%
\begin{figure}
\centering
\includegraphics[width=1\columnwidth]{figures/qk-dpu.pdf}
\caption{A QK-DPU comprising (a) bit-serial dot-product engine, (b) margin calculation logic, (c) thresholding module, and (d) score index counter, where bit-sequential resolution $\mathcal{B}=2$.}
\label{fig:qk-dpu} 
\end{figure}
%

\niparagraph{Bit-serial dot-product execution.}
% explain (a)
Figure~\ref{fig:qk-dpu}-(a) depicts the microarchitectural details of our Bit-Serial Dot-product Engine (\code{BS-DPE}).
%
The \code{BS-DPE} is a collection of Multiply-ACcumulate (MAC) units and it performs a 12-bit$\times \mathcal{B}$-bit dot-product operation per cycle, where the $\mathcal{Q}$ vector is kept in a local register and $\mathcal{K}$s are read from the \code{Key Buffer} $\mathcal{B}$-bit at a time in a sequential mode.
%
We chose $\mathcal{B}=2$-bit  as opposed to conventional bit-by-bit serial designs as the number of bits processed per cycle opens a unique trade-off space for the design of \sys.
%
Increasing the bits leads to better power efficiency due to less frequent latching of intermediate results, however it may degrade the performance as it reduces the resolution of  bit-level early  termination.
%
As such we perform a design space exploration (Figure~\ref{fig:bit_vs_power} in Section~\ref{sec:design_space}) and chose 2-bit serial execution as it strikes the right balance between power efficiency and performance.
%
The \code{BS-DPE} accumulates all the intermediate results in around 20 bits to keep required precision of the computations.
%
The output of the last 2-bit$\times$12-bit MAC unit then goes to a shifter to scale the partial results according to the current $\mathcal{K}$ bit position and is accumulated and stored in a register that holds the (partial) results of $Score$ computations.
%

\niparagraph{Pruning detection via dynamic margin calculation.}
% explain(b) and (c)
As discussed in Section~\ref{sec:early_stop} and Figure~\ref{fig:bit_serial_alg_overview}, to detect whether a current $Score$ needs to be pruned and corresponding computations be terminated, \code{QK-DPU} dynamically calculates a \emph{conservative upper-bound margin} ($\mathcal{M}$) and adds it with the current dot-product partial sum ($\mathcal{P}$) to compare it with the layer threshold ($\mathcal{T}h$).
%
Figure~\ref{fig:qk-dpu}-(b) and (c) show the details of hardware realization for margin calculation and thresholding logic, respectively.
%
To calculate the margin according to Table in Figure~\ref{fig:bit_serial_alg_overview}, the margin calculation module first detects the $\mathcal{Q}$ and $\mathcal{K}$ pairs in the dot-product that yield positive product.
%
To do so, during the processing of $\mathcal{K}$'s MSBs, the sign bits of $\mathcal{Q}$s and $\mathcal{K}$s are XORed.
%
Only if the result is a positive (XOR = 0),  the magnitude of corresponding $\mathcal{Q}$ $|\mathcal{Q}|$ is summed up to contribute the margin calculation (e.g., resulting in $(9+5)$ in the Table of Figure~\ref{fig:bit_serial_alg_overview}).
The summation result is stored in a  \code{Sum Register}. Then, it is  scaled by the fixed number, largest positive value (e.g. 0111...), which corresponds to  $(2^{-1}+2^{-2}+2^{-3} + ...)$ in Figure~\ref{fig:bit_serial_alg_overview}, storing $(9+5)(2^{-1}+2^{-2}+2^{-3} + ...)$ in margin register.
%
\begin{comment}
On the other hand, if it turns out the  multiplication yields a negative value (XOR = 1) during the processing of $\mathcal{K}$ MSBs, the computation is excluded and skipped toward the margin calculation, which is enabled via multiplication by zero (000...). 
\end{comment}
%
The margin needs to be calculated dynamically for each bit position during bit-serial execution (such as $\mathcal{M}$ changing in each row of the Table in Figure~\ref{fig:bit_serial_alg_overview}). This is enabled by subtracting the shifted version of \code{Sum Register} value  from the current margin in the margin register, e.g., $(9+5)(2^{-1}+2^{-2}+2^{-3} + ...) - (9+5)(2^{-1}) = (9+5)(2^{-2}+2^{-3} + ...)$ in the second row of the Table in Figure~\ref{fig:bit_serial_alg_overview}. This operation is iterated every bit position to generate the values in the subsequent rows of the Table in Figure~\ref{fig:bit_serial_alg_overview}.
% Given our choice of $\mathcal{B}=2$, two subtractions are required for each cycle.
Note that the  margin calculation is a simple scalar computation, mostly shift and subtraction, well amortized over the $d=64$ dimension vector processing incurring almost no overhead.
%
\begin{comment}
Note that, since the Queries need to be multiplied by either of the three aforementioned bit streams, these multiplications are implemented merely with Shift and Add, instead of complex and expensive multipliers.
Finally, the accumulated result of these operations yields the current margin.
\end{comment}
After each cycle of the bit-serial operation, the thresholding module (Figure~\ref{fig:qk-dpu}-(c)) adds the updated partial sum with the current margin and compares it with the layer threshold $\mathcal{T}h$ to determine the continuation of the dot-product or its termination for pruning of the current $Score$.
%

\niparagraph{Final score index calculation.}
% explain (d)
The \code{QK-DPU} calculates the indices of the unpruned $Score$s using a set of two counters, as shown in Figure~\ref{fig:qk-dpu}-(d).
%
The first one, \code{Bit-serial Cntr}, increments with the number of bits processed by the \code{QK-DPU} and gets reset whenever it reaches to its maximum (i.e. 6 (= 12bit/$\mathcal{B}$)) for the case of processing all bits for unpruned $Score$s) or the \code{Early stop} flag is asserted.
%
The second one is \code{IDX Cntr}, of which value shows the position of current $Score$ in the $Score$ vector and it increments whenever the \code{Bit-serial Cntr} gets reset, meaning the computation of that $Score$ is over.
%
Finally, if the \code{IDX Cntr} increments and the \code{Early stop} flag is low, the \code{QK-DPU} pushes the content of this counter to \code{IDX FIFO}, because it means that the corresponding $Score$ is not pruned and will be used for further processing in the \code{V-PU}.
%
\subsection{Back-end Value Processing for Non-pruned Scores}
%
As shown in Figure~\ref{fig:architecture}-(b), the \sys tile's back-end stage, \code{V-PU}, consumes the unpruned $Score$s and executes the Softmax operation, followed by multiplication with $\mathcal{V}$ vectors and finally stores the results on an \code{Output-FIFO}.
%
Whenever the \code{Score-FIFO} is not empty, the \code{V-PU} starts the Softmax operation ($e^x$ and accumulation) to produce the probability.
%
We implemented the Softmax module of \code{V-PU} following the Look-Up-Table (LUT)-based methodology presented in \aaa~\cite{a3:hpca20}.
%
Whenever the output probability is produced, the \code{V-PU} uses the indices of the unpruned $Score$s to access the \code{Value Buffer} and reads the corresponding $\mathcal{V}$ vector.
%
Finally, the $\mathcal{V}$ vector is weighted by the output (probability) of the Softmax module with an 1-D array of MAC units, where the elements of $\mathcal{V}$ vector are distributed across the MAC units and the scalar probability is shared amongst them, similar to a 1-D systolic array.
%
With such design, the \code{V-PU} consumes the $Score$ values sequentially to complete the weighted-sum of $\mathcal{V}$ vectors, and accumulates the partial results over multiple cycles while only accessing the unpruned $\mathcal{V}$ vectors.
%
As such, it rightfully leverages the provided pruning by the front-end stage and eliminates the in-consequential computations of the less important $Score$s.






%%%%% Needed for throughput matching stuff or when we talk about *V part %%%%%
% Note that 65 - 97\% of scores will be pruned-out during the bit-sequential processing of Q*K. Therefore, we equip multiple (six) QK processors for the parallel processing while only a single back-end processor for the softmax and *V processor are shared across the multiple QK processors.
%


% \subsection{Throughput matching between front- and back-end stages}
% In this architecture, we chose the bit-precision of 12-bit for key vectors not to degrade the accuracy.  In the worst case, the outputs of all the six QK processors are not pruned out even after 12 cycles for the entire bit processing. In this case, six un-pruned outputs are transferred to the score FIFO  for further processing in the back-end processor.  Therefore, the back-end processor should be able to process maximum 6 input (score) vectors every 12 cycles, i.e., the softmax and *V processor should be able to complete their operations within two cycles not to create the throughput bottleneck at the back-end processor. The value memory should be able to support the throughput with wide memory bandwidth accordingly.  In most of the cases, however, only one or two of six QK processors produce the un-pruned outputs statistically as the pruning rate is 65 - 97\%. Thus, the expensive back-end processing with softmax block, value memory and *V processor operate less frequently than in the worst case.


% The QK bit-sequential processor includes two blocks: 1) sign-bit processor which computes $Q_{pos\_sum}$, and 2) magnitude-bit processor for the following bits.
% The sign-bit processor operates only during the 0-th (sign) bit processing to compute the vector $\mathrm{idx}_{pos}$ by multiplying the MSBs (sign bits) of queue and key vector elements in parallel for all the vector elements. In addition, the $Q_{pos\_sum}$ is computed by summing all the magnitude values of queue vector elements at the location of $Q_{pos\_sum}$='1'. The computation of both $\mathrm{idx}_{pos}$ and $Q_{pos\_sum}$ are processed in a single cycle for the 0-th bit processing. In the subsequent cycles for the remaining bit precision, the magnitude-bit processor performs the dot-product between bit-sequential binary vector of key and decimal vector of queue. The result is  shifted (to represent the binary scale according to bit position of key), and accumulated with previous stage's psum. In each bit stage, the psum is compared  with the  "$th - margin(b)$" based on (\ref{eqn: decision} to check the early-stop possibility. Note that the same $th$ parameter is shared for the entire QK vector combinations and head in the same layer as described in \ref{sec: train_method}. Thus, the value of "$th - margin(b)$" can be pre-calculated and used from the look-up-table for each layer.  