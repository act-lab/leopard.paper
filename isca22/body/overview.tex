\section{Background and Motivation}
\label{sec:overview}

% Attention mechanism
\subsection{Self-Attention Mechanism}\label{subsec:overview:self-attention}
% What is the high-level goal of self-attention mechanism?
At high-level, ``\textit{self-attention}'' is a mechanism to find the relation between a word to all the other words in a sentence~\cite{vaswani2017attention,performer}.
%
To compute this relation, we first project each word to a vector with $d_w$ dimensions, so-called embedding.
%
Given a sentence with $s$ words, this projection creates a matrix $\mathcal{X}$ with $s\times{d_w}$.
%
Then, these word embeddings are multiplied into query weight matrix ($W^\mathcal{Q}$), key weight matrix ($W^\mathcal{K}$), and value weight matrix ($W^\mathcal{V}$), each with $d_w\times{d}$ dimensions as follows:
\begin{equation}
\label{eq:qk}
    \mathcal{Q}_{s\times{d}} = \mathcal{X}\times\mathcal{W^\mathcal{Q}};\quad\mathcal{K}_{s\times{d}} = \mathcal{X}\times\mathcal{W^\mathcal{K}};\quad\mathcal{V}_{s\times{d}} = \mathcal{X}\times\mathcal{W^\mathcal{V}}\\
\end{equation}
%
\noindent{}Given the query ($\mathcal{Q}$) and key ($\mathcal{K}$) matrices, a self-attention $Score$ matrix is calculated as follows:
\begin{equation}
\label{eq:score}
   \mathrm{Score}_{s\times{s}} = \mathcal{Q}\times\mathcal{K}^T
\end{equation}
%
\noindent{}where each element $s_{ij}$ in the self-attention $Score$ matrix indicates the relation between word$_i$ and word$_{j}$ in the input sentence.
%
The $Score$ values are generally scaled down by a function of embedding size ($\times 1/\sqrt{d}$) before the next step to enable stable gradients during training~\cite{vaswani2017attention}.
%

To ensure that the self-attention $Score$s are positive and adding up to one, a ``\textit{softmax}'' operation is applied to each row of $\mathrm{Score}$ matrix as follows:
\begin{equation}
\label{eq:prob}
\begin{split}
%   \mathcal{D}_{s\times{}s} &= exp(\mathrm{Score})\\
  \mathcal{P}_{s\times{s}} &= \mathrm{Softmax}(\mathrm{Score})
%   \mathcal{D}\times[\mathrm{diag}(\mathcal{D}\times\overrightarrow{\mathbbm{1}}_{s\times{}1}))]^{-1}
\end{split}
\end{equation}
%
% \noindent{}where $\overrightarrow{\mathbbm{1}}$ represents a column vector of ones, $exp$ is the element-wise exponential operation, and the outcome of $\mathrm{diag}(\cdot)$ operator is a diagonal matrix with diagonal elements taken from the input vector.
%
\noindent{}The output of softmax operation indicates a $\mathrm{probability}$ estimation of the relation between the input words.
%
Finally, the self-attention values are calculated as follows:
%
\begin{equation}
\label{eq:attention}
   \mathrm{Att}_{s\times{}d} = \mathcal{P}\times\mathcal{V}
\end{equation}

Generally, each attention layer consists of multiple heads each with dedicated $W^\mathcal{Q}$, $W^\mathcal{K}$, and $W^\mathcal{V}$ weight matrices. Each head presumably captures different dependencies between the token embeddings.
%
In this case, the attention values  (Equation~\ref{eq:attention}) from each head are concatenated and projected into an attention matrix of size $s\times{}d_w$ using a weight matrix $\mathcal{W}^o_{({d}\times{}h)\times{d_w}}$ as follows:
\begin{equation}
\label{eq:multi-attention}
 \mathrm{Multi-Head\;Att}_{s\times{}d_w} = \mathrm{Concat}(\mathrm{Att}_1,\ \mathrm{Att}_2,\ \cdot\cdot\cdot,\ \mathrm{Att}_h)\times\mathcal{W}^o
\end{equation}
%
\noindent{}where $\mathrm{Concat}$ operation concatenates the $\mathrm{Att}$ output matrix from each head to generate a $(s\times{}(d\times{}h))$-matrix.

%
% As such, algorithmic and/or hardware optimization of self-attention operation have been the focus of many recent work~\cite{spatten:hpca21,a3:hpca20,stevens2021softermax,linformer:2020,longformer:2020,performer}.
% %

\subsection{Gradient-Based Optimization and Regularization}\label{subsec:overview:loss}
% How is the loss function generally calculated for Attention layers?
\niparagraph{Gradient-based optimization.}
%
Training neural networks are framed as an optimization problem of a loss function.
%
These loss functions are generally non-convex and have a manifold consisting of different local optima which makes the training of neural networks challenging.
%
To alleviate the complexity of optimizing loss functions, it is common to use gradient-based methods~\cite{robbins1951stochastic,kingma2014adam}. 
%
Using these gradient-based methods institute defining differentiable loss functions, such as cross-entropy~\cite{murphy2012machine} or Kullback-Leibler divergence~\cite{kullback1951information} which is prevalent in self-attention models~\cite{vaswani2017attention,memn2n:nips:2015,gbert:naacl:2019}.

\niparagraph{Regularization in loss function.}
%
To impose certain constraints on the model parameters, such as improved generalization~\cite{krogh1992simple,zou2005regularization,srivastava2014dropout} and introducing sparsity~\cite{srinivas2017training,gale2019state,learnedthreshold}, it is common to use regularizer as part of the loss function.
%
However, employing gradient-based methods for training mandates these regularizers to be framed as additional differentiable terms to the loss function.
%
This differentiability constraint for employing gradient-based methods introduces a unique challenge for supporting certain constraints that are not inherently differentiable as part of loss functions.

\subsection{Motivation}
% \niparagraph{Self-attention cost analysis.}
%
Analyzing the computations for self-attention layers, it is apparent that the main computation cost is associated to $\mathrm{Score}$ (Equation~\ref{eq:score}) and attention computations (Equation~\ref{eq:attention}) that necessitates the multiplications of two matrices with $s\times{}d$ dimensions, each with time complexity of $\mathcal{O}(s^2d)$.
%
%In addition, the entire $\mathrm{Score}$ matrix must be stored before applying the softmax operation.
%
These time and space complexity of self-attention calculations translate to quadratic raise in computation cost and storage as the number of input tokens increases.
%
As such, prior work aims to reduce the time and space complexity of these operations both from the algorithmic~\cite{performer,linformer:2020,longformer:2020,child2019generating,bigbird:2020} and hardware perspective~\cite{a3:hpca20,spatten:hpca21,elsa:isca21,kao2021optimized,stevens2021softermax}.
%
In this work, we propose an alternative pruning mechanism that learns the threshold as part of training.
%
Our proposed technique prunes away unimportant $Score$ values, hence eliminating the ineffectual computations of attention operation (Equation~\ref{eq:attention}). 
%
In addition, to further cut down the computations of $Score$ values (Equation~\ref{eq:score}), we employ a unique early-compute termination without negatively impacting the model accuracy.

% Regularizers
% How does self-attention mechanism work?
% How does multi-head self-attention mechanism work?
% Attention mechanism is costly.
% One way is to prune the results
% The attention layer measures how closely two tokens are related, e.g., two words in a sentence as described in Fig.~\ref{fig:pseudo} with pseudo code.  Once a new input including $N$ tokens is given,  each token in the input is converted to a $d$-dim vector, so-called embedding ($X_n$). Then, three additional $d$-dim vectors, query ($\mathrm{Q_n}$), key ($\mathrm{K_n}$) and value ($\mathrm{V_n}$), are created from each embedding ($X_n$) by matrix multiplications with $W_QX_n$, $W_KX_n$, and $W_VX_n$, respectively. To create an attention output ($\mathrm{Z_n}$) for the $n$-th token, the token's query vector ($\mathrm{Q_n}$) is dot-producted with all key vectors ($K_{1,...,N}$) to produce the $score_n$, which is divided by $\sqrt{d}$ to be normalized. Then, the $score_n$ goes through the softmax  and the weighted sum of value ($\mathrm{V_n}$) vectors with the softmax outputs generates the $n$-th attention output ($\mathrm{Z_n}$). This process is iterated for all the tokens to produce ($Z_{1,...,N}$). The same computation steps are iterated for multiple (e.g., 8 - 16) heads in the layer and also in multiple (e.g., 8 - 16) attention layers. Therefore, total 64 - 256 attention computations are required leading to dominant  portion of latency and power consumption in the entire model as pointed in \cite{spatten:hpca21, a3:hpca20}. 





%
% \begin{figure*}
% \begin{minipage}{1\linewidth}
% 	\centering
% 	\vspace{-1ex}
% 	\includegraphics[width=0.7\linewidth]{figures/nlp-accel-comparison.pdf} 
% 	\vspace{0.5ex}
% 	\caption{Comparison of \sys to prior algorithm-hardware co-design approaches.\amir{update table---maybe a paragraph.}}
% 	\label{fig:accel-comp}
% 	\vspace{-1ex}
% \end{minipage}
% \vspace{-2ex}
% \end{figure*}

% \begin{table}[t!]
% \footnotesize{
% \centering
% \vspace*{0.1cm}
% \caption{\label{table:accel-comp}Comparison of \sys to prior algorithm-hardware co-design approaches.\amir{update table---maybe a paragraph..}}
% \resizebox{0.49\textwidth}{!}{% <------ Don't forget this %
% \begin{tabular}{l|c|c|c|c|c}
% \bottomrule
% \textbf{Features / Designs}&\textbf{A$^\mathrm{3}$}~\cite{a3:hpca20}&\textbf{ELSA}~\cite{elsa:isca21}&\textbf{SpAttn}~\cite{spatten:hpca21}&\textbf{Sanger}~\cite{sanger:micro21}&\textbf{\sys}\\\bottomrule
% \textbf{Early Pruning}&\checkmarkgreen{}&\checkmarkgreen{}&\checkmarkgreen{}&\checkmarkgreen{}&\checkmarkgreen{}\\\midrule
% \textbf{Low-Overhead Pruning Execution}&\crossred{}&\checkmarkgreen{}&\crossred{}&\crossred{}&\checkmarkgreen{}\\\midrule
% \textbf{Fine-Grain Unstructured Pruning}&\checkmarkgreen{}&\checkmarkgreen{}&\crossred{}&\crossred{}&\checkmarkgreen{}\\\midrule
% \textbf{Learned Pruning}&\crossred{}&\crossred{}&\crossred{}&\crossred{}&\checkmarkgreen{}\\\midrule
% \textbf{Bit-Level Early Compute Termination}&\crossred{}&\crossred{}&\crossred{}&\crossred{}&\checkmarkgreen{}\\\midrule
% \end{tabular}% <------ Don't forget this %
% }
% }
% \end{table}



% Amir: we should discuss this in the intro/motivation
% The self-attention mechanism  is a key computing kernel in many state-of-art NLP models such as MeMN2N, BERT, Albert, and Roberta \cite{memn2n:nips:2015, beret, albert:iclr:2019, roberta:arxiv:2019}. It has been well demonstrated that the attention block is the most power and latency hungry block in hardware implementations \cite{a3:hpca20, spatten:hpca21, elsa:isca21} as it is iterated in many layers, e.g., 12-16, and each self-attention block includes multiple 8-16 heads internally.




% \begin{figure}
% \includegraphics[width=0.85\columnwidth]{figures/attention_example_fig.pdf}



% //////// Pseudo code /////////

%  // head index: $h$ ($h\in H$, e.g., $H$: 12 - 24 heads)
 
%  // token index: $n$ ($n$ $\in N$, e.g., $N$: 128 - 512 sequence length)
 
%  // queue, key, value element index: $d$ ($d\in D$, e.g., 64 vector length)
% \\

% for $h$ = 0 to $H$-1 

% \hspace*{3mm}   for $n_1$ = 0 to $N$-1 
   
% \hspace*{6mm}   for $n_2$ = 0 to $N$-1
     
% \hspace*{9mm}   for $d$ = 0 to $D$-1
       
% \hspace*{12mm}  $score$  += queue[$n_1$][$d$]*key[$n_2$][$d$]
          
% \hspace*{12mm}  $prob$[$n_2$] =  softmax[psum/sqrt($D$)]
          
% \hspace*{9mm}   $out$[$n_1$][:] += $prob$[$n_2$]*value[$n_2$][:]

% \caption{Attention mechanism\all{Add a proper algorithm here}\amir{I can provide a template}.}
% \label{fig:pseudo} 
% \end{figure}









% \soroush{it will be nice to have a study that analyzes the breakdown of computations across transformer computations to show potential benefits of this work and also compare it at a high level with prior work.}


% \color{purple} Mingu: Fig.3 has been added.\color{black}
% Figure~\ref{fig:sentence_redundancy} shows the computation of attention mechanism with an example of human sentence. As understood intuitively, many of words in a sentence are loosely connected each other, e.g., Ellie vs. Morrill. Moreover, many words in a natural human sentence are redundant being hardly related to any other words in the sentence. This unique nature makes  large portion of  elements in a $score$ vector to be near zero. The softmax function even magnifies the gap between large and small numbers so that the large values get even larger whereas the small values becomes near zero (the 3rd last row of Fig.~\ref{fig:sentence_redundancy}). The recent research \cite{elsa:isca21, spatten:hpca21, a3:hpca20} shows the near-zero  elements in the matrix below a certain  threshold can be pruned out and excluded for further processing without causing accuracy degradation (the 2nd last row of Fig.~\ref{fig:sentence_redundancy}). 

% However, there are multiple challenges in exploiting  this opportunity. Firstly, during the pruning process to remove the small elements in the $score$ vector, it is non trivial to find the optimal threshold which maximizes the pruning rate while maintaining the original accuracy. One can search the optimal value by sweeping the threshold  with the training data set. However, it is impractical to find those in a brute-force manner for all the 256 self-attention kernels across layers and heads.
% An alternative approach applied in \cite{a3:hpca20, elsa:isca21} is finding the maximum element in the $score$ vector, and employ $maximum * p$ as the threshold, where $p$ is a hyper parameter. However, this approach has following drawbacks: 1) the $p$ reflects the confidence level and is supposed to be decided by the user to control the level of approximation. However, the same $p$ value causes different accuracy degradation across the testbenches and NLP models as shown in \cite{a3:hpca20}. Therefore, this approach leaves the big responsibility of selecting the right $p$ value to users. 2) Another disadvantage is, it is hard to know whether the current element in the  $score$ vector will be pruned out or not until  $Q_n$ vs. all the $K_{n=1,2,...N}$ are computed to find the maximum element. Therefore, all the elements in the $score$ vector need to be stored and cannot move onto the next stage before the maximum value is found for the pruning.

% Our objective in this paper is \textbf{finding the redundant portion of computation as early as possible to minimize  the computations and associated memory access without approximation}. Our contributions in this paper are as follows: 1) an online-pruning is employed to skip the redundant tokens during the  $Q_n*K_n$ computations, which avoids the  costly subsequent processing stages such as softmax and multiplication with value ($V$) matrix.
% 2) to enable the early-stage pruning before the softmax, the optimal threshold for pruning and weights are learned during the training process, which co-optimize the accuracy and pruning rate together. This process does not require the hyper parameter to adjust  the level of approximation,
% 3) bit-wise early-termination is supported during the bit-sequential $Q_n*K_n$ processing. If it turns out the current token cannot be larger than threshold during the bit sequential processing, the processing of $Q_n*K_n$ is early-terminated to save the energy and latency of the computation and associated  memory access for the remaining bits.
