\section{Evaluation}
\label{sec:eval}

\subsection{Methodology}
\label{subsec:methodology}

\niparagraph{Workloads.}
To evaluate the efficacy of \sys, we use following NLP models: Google \bench{BERT-Base} (\bench{BERT-B})~\cite{gbert:naacl:2019}, Google \bench{BERT-Large} (\bench{BERT-L})~\cite{gbert:naacl:2019}, and Facebook \bench{MemN2N}~\cite{memn2n:nips:2015}.
%
For these models, we use three different datasets: 1) Facebook \bench{bAbI}, which includes 20 different tasks~\cite{babi:arxiv:2015} for \bench{MemN2N}, 2) General Language Understanding Evaluation (\bench{GLUE}) with nine different tasks~\cite{glue:arxiv:2018} for \bench{BERT} models, and 3) Stanford Question Answering Dataset (\bench{SQUAD}) \cite{squad:arxiv:2016} with a single task for \bench{BERT} models.
%
The dimension ($d$) of $\mathcal{Q}$, $\mathcal{K}$, and $\mathcal{V}$ vectors for all the workloads is 64 except \bench{MemN2N} with \bench{bAbI} dataset, which is 20. The sequence length is 50 for \bench{MemN2N} with \bench{bAbI} whereas 512 and 384 for \bench{BERT} models with \bench{GLUE} and \bench{SQUAD} datasets, respectively.
%

\niparagraph{Fine-tuning details.}
%
We use the baseline model checkpoints from HuggingFace~\cite{huggingface:2019} with PyTorch v1.10~\cite{pytorch} and  fine-tune the models on an Nvidia RTX 3090 GPU.
%
For default task-level training, we use Adam optimizer with default parameters and learning rate $2 - 3e^{-5}$  (same as baseline transformer training).
%
To obtain the layer-specific threshold values, we perform an additional pruning-aware fine-tuning step for one to five more epochs to learn the optimal values that still preserve the baseline model accuracy.
%
For this step, we use a learning rate of $1e^{-2}$ for $\mathcal{T}h$ ($5e^{-6}$ for  the other parameters), as training for the $\mathcal{T}h$ is generally slower and a higher learning rate facilitates the convergence process.
%
To leverage faster fixed-point execution, we perform a final post-training quantization step with 12 bits for inputs in \code{QK-PU} hardware block and 16 bits for \code{V-PU} block following the methodology presented in \cite{spatten:hpca21}. 

% Note that, as \sys learns the clipping value as part of the post-training step, the user do not need to perform additional tuning for the threshold values. 
% %
% To obtain the optimal clipping value and input/output bit-width while preserving the SOTA accuracy, we perform a post-training quantization step (\amir{similar methodology as~\cite{} -- maybe cite Spatten}).
%
\begin{table}[t!]
\footnotesize
\centering
\vspace*{0.1cm}
\caption{Microarchitectural configurations of a \sys tile.}
\resizebox{0.48\textwidth}{!}{% <------ Don't forget this %
\begin{tabular}{l|l}
\toprule

\textbf{Hardware modules} & \textbf{Configurations} \\\midrule
QK-PU & 6 / 8 \code{QK-DPU}  (=$N_{\mathrm{QK}}$), each 64 (=$\mathcal{D}$) tap 12$\times$2 bit-serial  \\\hline
Key Buffer& 48KB in total (= 8KB$\times$6 / 6KB$\times$8 banks), 128-bit port per bank \\\hline
V-PU & Single 1-D 64 (=$\mathcal{D}$) way 16$\times$16-bit MAC array\\\hline
Value Buffer & 64KB (= 8KB $\times$ 8 banks), 128-bit port per bank \\\hline
Softmax & 24-bit input,  16-bit output, LUT: 1 KB\\\hline
Score and IDX FIFOs & 24-bit $\times$ 512 depth for Score, 8-bit $\times$ 512 depth for IDX\\
\bottomrule
\end{tabular}% <------ Don't forget this %
}
% \vspace{-0.5cm}
\label{table:arch_config}
\end{table}
%

\niparagraph{Hardware design details.}
%
Table~\ref{table:arch_config} lists the microarchitectural parameters of a single \sys tile for two studied configurations:
%
(1) A \sys tile with six and (2) eight \code{QK-DPU}s working in parallel, while sharing a single 1-D MAC array in \code{V-PU}.
%
The number of \code{QK-DPU}s is set such that the compute utilization for front-end and back-end units is balanced, while considering the pruning and bit-level early-termination rates across all the workloads.
%
Section~\ref{sec:design_space} discusses this matter in more details.
%
We synthesised and performed Placement-and-Route (P\&R) for our designs with two tiles.
% \sys architecture supports up to 512 sequence length to cover all the models listed in Table~\ref{tab: benchmarks}.
%
The on-chip memory sizes for $\mathcal{K}$ and $\mathcal{V}$ are decided to store up to 512 sequences for a single head in a layer for both configurations.
%
% To design the \sys accelerator (see Figure~\ref{fig:architecture}), we employ 48~KB key memory and 64~KB value memory per head block to store 512 sequences.
%

\niparagraph{Accelerator synthesis and simulations.}
We use Cadence Genus 19.1~\cite{genus} and Cadence Innovus 19.1~\cite{innovus} to perform logic synthesis, floorplan, and P\&R  for the \sys accelerator.
%
We use TSMC 65~nm GP (General Purpose) standard cell library for the synthesis and layout generation of the digital logic blocks.
%
These digital blocks are rigorously generated to meet the target frequency of 800MHz in consideration of  all the CMOS corner variations (e.g. slow, typical, and fast) and temperature condition from -40 to $125^{\circ} \mathrm{C}$.
%
For the SRAM on-chip memory blocks, we use  Memory Compiler with ARM High density 65~nm GP (General Purpose) 6-transistor based single-port SRAM version r0p0 \cite{mem_compiler}.
%


% Cycle accurate
% memory access, read or write
% energy each read and write, compute.
% We also develop a cycle-accurate simulator for \sys accelerator that maps the workloads to \sys architecture and provides the total cycle counts and number of accesses to memories in consideration of the pruning rate and bit-level early-termination statistics for each individual workload.
%
We also develop a \sys simulator to provide the total cycle counts and number of accesses to memories for both \sys and baseline accelerators in consideration of the pruning rate and bit-level early-termination statistics for each individual workload.
%
Then the simulator uses these statistics to evaluate the runtime and total energy consumption of running workloads on the accelerator.
%
% We verify the simulator cycle counts with our Verilog implementations.
% %



\niparagraph{Comparison to Baseline architecture.}
%
To evaluate the benefits of our proposed algorithmic and architectural optimizations, we compare \sys to a conventional baseline design without any of our optimizations (e.g. runtime pruning and bit-level early compute termination).
%
For fair comparison, we use the same frequency, bitwidths for $\mathcal{Q} \times \mathcal{K}$ and $\times \mathcal{V}$, and on-chip memory capacity for both baseline and \sys designs.
%
The baseline employs a single 12$\times$12-bit \code{QK-DPU} as opposed to multiple 12$\times$2-bit-serial ones, while both designs have the same back-end \code{V-PU}.
%
As also listed in Table~\ref{table:arch_config}, we use two design configurations.
%
The first design with six \code{QK-DPU}s, dubbed Area-Efficient \sys (\aeleopard), almost perfectly matches the area of the baseline design (< 0.2\% area overhead) and provides an iso-area comparison setting.
%
We also consider an alternative configuration for \sys with eight \code{QK-DPU}s, dubbed Highly-Parallel \sys (\hpleopard), with a comparable area to the baseline (15\% larger) that provides a better balance in the compute utilization of the front-end and back-end stages. 
%
%
% Describe our baseline with no optimization (Is it 12 bits or full precision?)
% Describe A3
% Why A3 makes sense for comparison? Describe the comparison methodology.

\niparagraph{Comparison to \aaa and SpAtten.}
%
We also compare \sys with two state-of-the-art attention accelerators, \aaa~\cite{a3:hpca20} and SpAtten~\cite{spatten:hpca21}, with support for runtime pruning.
%
\aaa employs the token pruning after computing softmax by comparing the softmax output ($\mathrm{probability}$) to a relative threshold, which is set using a user-defined parameter that adjusts the level of approximation.
%
\aaa also employs a sorting mechanism to make a decision for the pruning after processing only a small number of large elements from the sorted  $\mathcal{K}$ matrix in the  magnitude order.
%
% they also skip the QK computations through a sorting step which eliminates the inconsequential outputs
%
% \aaa also minimizes the $\mathcal{Q} \times \mathcal{K}$ computations by computing the limited number ($m$) of elements with a large magnitude in $\mathcal{K}$ matrix, where $m$ is another user-defined approximation parameter.
%
SpAtten performs cascaded head and token pruning after the softmax computation by comparing with a user-defined threshold obtained empirically.
%
Due to the lack of available raw performance/energy results for individual workloads and simulation infrastructures of the accelerators, commensurate with comparison methodology of SpAtten~\cite{spatten:hpca21}, we use throughput (GOPs~/~s), energy efficiency (GOPs~/~J), and area efficiency (GOPs~/~s~/~mm$^2$) metrics to provide the best effort analyses.
%
% We calculated the number of operations required for each generated $Score$ for all accelerators as follows: $\mathcal{Q} \times \mathcal{K}$ and $\times \mathcal{V}$ processing: $2\times d$ operations, where 2 is from MAC and softmax: $3$ operations from $e^x$, accumulation, and division.
% %
Both \aaa and SpAtten are implemented in 40~nm technology. 
%
To provide a fair comparison, we scale \hpleopard from 65~nm to 40~nm and use a single tile of it that has a comparable area to \aaa and SpAtten.
%
Moreover, \aaa implements the $\mathcal{Q} \times \mathcal{K}$ using 9 bits as opposed to 12 bits in this work.
%
As such, we also scale the \code{QK-PU} of \hpleopard from 12 bits to 9  bits to provide a head-to-head comparison with \aaa. 
%while also analyzing its effect on the accuracy of the models.
%



\begin{comment}
Though \aaa does not report absolute performance and energy numbers, \aaa takes $\mathcal{S}$ (and $\mathcal{S}/2$) cycles in the candidate selection stage to process the multiplication between ($S \times d$)-dim $\mathcal{K}$ matrix and $d$-dim $\mathcal{Q}$ vector in  \aaa base (and \aaa conservative).

As reported in \cite{a3:hpca20}, the throughput of \aaa is limited by the candidate selection stage. Given this factor with the reported clock frequency of 1 GHz and power consumption of 110.4W, the performance and energy consumption are estimated. 


\sys equips large enough on-chip memory capacity (48KB + 64KB) to store entire $\mathcal{K}$ and $\mathcal{V}$ vectors required for a single head processing. However, we built  \sys projected$^1$ version by scaling on a 40~nm process technology with a comparable memory capacity (24KB + 24KB) to the other accelerators for fair comparison. we also generated  \sys projected$^2$ version which employs 9bit QK processing for  comparison  though \sys supports up to 12 bits. \sys projected$^1$ achieves 1.6$\times$ higher throughput per unit area and 2.6$\times$ higher energy efficiency than Spatten. \sys projected$^2$ also  achieves 4.1$\times$ higher throughput per unit area and 1.3$\times$ higher energy efficiency than \aaa (base).  \aaa (conservative) achieves higher throughput and energy efficiency by allowing some approximations at the cost of  accuracy degradation with 1.3\%.
%
Due to the tight combination of on-line learned pruning and bit-wise early-termination, \sys prunes out  redundant tokens with high pruning rate at early stage before softmax and even before the entire QK processing without pre-processing steps and user-defined empirical parameters.
\end{comment}

%
\begin{comment}
\begin{table}[t!]
\footnotesize{
\centering
\vspace*{0.1cm}
\caption{\label{table:peak_throughput} Peak throughput / area at different granularity level of early-compute termination. $\mathcal{B}$ indicates the maximum number of bits that are used for QV operation before the computation terminates.\amir{Is this still correct? We have changed the results.}}
% \resizebox{0.48\textwidth}{!}{% <------ Don't forget this %
\begin{tabular}{l|C{1.5cm}|C{.7cm}|C{.7cm}|C{.8cm}|}
\cline{2-5}
&&\multicolumn{3}{c|}{\textbf{\sys}}\\\cline{3-5}
&\textbf{Baseline Accelerator}&\textbf{$\mathcal{B}$ = 4}&\textbf{$\mathcal{B}$ = 8}&\textbf{$\mathcal{B}$ = 12}\\\bottomrule
\multicolumn{1}{|c|}{\textbf{Peak Throughput / Area}}&206.7&981.3&490.6&327.1\\\hline
\end{tabular}% <------ Don't forget this %
% }
}
\end{table}
\end{comment}
%

% In this section, we evaluate the benefit of proposed work by comparing the baseline architecture, which is similar to the proposed architecture in Fig.~\ref{fig:architecture}, but without the early-stop and on-lining pruning capability. 
% Thus, the baseline architecture has a 12-bit QK processor (64-tap dot product engine) instead of six bit-sequential processors. 
% On the other hand, the 12-bit QK processor completes the computation within 8ns (125 MHz) based on the post-layout simulations while the 2b sequential processor takes 4ns leading to 24ns to complete the entire 12-bit processing. Despite the slow processing due to the sequential nature, the proposed work achieves higher throughput, power, and area efficiency due to the online-pruning and early-stop capability as will be discussed in the following sections. 
% All the digital logic blocks are synthesized with Cadence Genus, and the layers are generated by performing the placement-and-routing with Cadence Genus. 
% All the digital blocks are generated by strictly meeting the target frequency in slow, typical and fast corner variations for the robust operation in a 65 nm CMOS process technology. 
% The on-chip memory (SRAM) blocks are generated via the memory compiler with a 6-transistor based single-port SRAM.
% \subsection{Methodology}\label{sec:methodology}
% We employed three different models, MemN2N, BERT base, and BERT large to validate the efficacy of the proposed scheme across different sizes of models. 
% We evaluated two different datasets 1) Facebook babi dataset [ ], which includes 20 different tasks, and 2) GLUE dataset [ ], which includes nine tasks. 
% We applied the post-training quantization for the inputs for each processing stage by finding the optimal clipping value and minimum bit precision not to degrade the accuracy leading to 12 and 16 bits for the QK procesor and *V processor, respectively, which is similar to [spatten]. 
% On the other hand, the $score$ (the output of QK processor) and the output of *V processor requires 24 and 20 bits not to degrade the classification accuracy.
% We employed 48KB key memory and 64 KB value memory per each head processor in Fig.~\ref{fig:architecture} to store 512 sequences. 
% \amir{how about providing some pareto for accuracy-performance trade-off? (maybe 0.1, 0.2, ..., 1.0 percent accuracy loss?}
% \color{orange} Mingu: I also thought this direction, but found it is very hard. First of all, there are many knobs to reduce the accuracy. Even for reducing bit precision, there are many places to control the bit precision, Q, K, V, QK, softmax output. In addition, the trained accuracy fluctuates so it is hard to know that the 0.1\% accuracy drop comes from the bit reduction or just bad luck.\color{black}
% \amir{If we are mentioning spatten, could it be a baseline for us? or at least we should discuss it?}
% \color{orange} Mingu: We could mention Spatten in the related work section, but not as a baseline.\color{black}